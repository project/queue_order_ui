CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This is the module that provide UI to control sorting of
queue workers definitions.
Go to /admin/config/system/cron/queue-order to use the sorting form.

 * For a full description of the module visit:
   https://www.drupal.org/project/queue_order

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/queue_order


REQUIREMENTS
------------

This module require [Queue Order](https://www.drupal.org/project/queue_order)
module.


INSTALLATION
------------

 * Install the Queue order module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.


CONFIGURATION
-------------

No configuration.

MAINTAINERS
-----------

 * Oleh Vehera (voleger) - https://www.drupal.org/u/voleger

Supporting organization:

 * GOLEMS GABB - https://www.drupal.org/golems-gabb

GOLEMS GABB is a team of experienced developers! 
Our work includes strategy, design, and development
across a variety of industries.
